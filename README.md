# Irrigation Tank Analysis

This anonymous repository is intended for use in the peer-review of the associated manuscript only.

The missing `NDWI_reproject.tiff` dataset from the `data/` directory (omitted as it is very large, >800MB) is available for download [here](https://www.dropbox.com/s/inhgxbp0g9lzyww/NDWI_reproject.tiff?dl=1).